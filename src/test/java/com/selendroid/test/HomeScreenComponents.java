package com.selendroid.test;

import org.testng.annotations.Test;
import setup.AndroidAppiumTest;

public class HomeScreenComponents extends AndroidAppiumTest {
    HomeScreenComponentsPageObject homeScreenObject;

    @Test (priority = 1)
    public void testClickOnEnButton()
    {
        homeScreenObject=new HomeScreenComponentsPageObject(AndroidDriver);
        homeScreenObject.ClickOnEnButton();
    }
    @Test (priority = 2)
    public void testClickOnWebViewButton()
    {
        homeScreenObject=new HomeScreenComponentsPageObject(AndroidDriver);
        homeScreenObject.ClickOnWebViewButton();
    }
    @Test (priority = 3)
    public void testSendTextToTextFiledSection()
    {
        homeScreenObject=new HomeScreenComponentsPageObject(AndroidDriver);
        homeScreenObject.SendTextToTextFiledSection();
    }
    @Test (priority = 4)
    public void  testShowProgressBarForAwhile() throws InterruptedException {
        homeScreenObject=new HomeScreenComponentsPageObject(AndroidDriver);
        homeScreenObject.ShowProgressBarForAwhile();
    }
    @Test (priority = 5)
    public void testUnCheckCheckedBox()
    {
        homeScreenObject=new HomeScreenComponentsPageObject(AndroidDriver);
        homeScreenObject.UnCheckCheckedBox();
    }
    @Test (priority = 6)
    public void testDisplayTextBoxButton()
    {
        homeScreenObject=new HomeScreenComponentsPageObject(AndroidDriver);
        homeScreenObject.DisplayTextBoxButton();
    }
    @Test (priority = 7)
    public void testClickOnDisplayToastButton()
    {
        homeScreenObject=new HomeScreenComponentsPageObject(AndroidDriver);
        homeScreenObject.ClickOnDisplayToastButton();

    }
    @Test (priority = 8)
    public void testClickOnShowPopUpButton()
    {
        homeScreenObject=new HomeScreenComponentsPageObject(AndroidDriver);
        homeScreenObject.ClickOnShowPopUpButton();

    }

}
